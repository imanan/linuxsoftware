#!/bin/bash

echo "\n\033[33m#################### Start ####################\033[0m \n "

sudo apt-get update

echo "\033[33m##############################################\033[0m"
echo "\033[33m#           \033[39mStart Install Software           \033[33m#\033[0m"
echo "\033[33m##############################################\033[0m \n"

#set gedit display chinese
gsettings set org.gnome.gedit.preferences.encodings candidate-encodings "['GB18030', 'UTF-8', 'CURRENT', 'ISO-8859-15', 'UTF-16']"

sleep 0.3

sudo apt install screen -y
sudo apt install net-tools -y
sudo apt install inkscape -y
sudo apt install goldendict -y


sleep 0.3

# install openssh-server
dpkg -l | grep openssh-server >> /dev/null
if [ $? -eq 0 ]
then
echo "openssh-server Is already installed \n"
else
sudo apt -y install openssh-server
sudo /etc/init.d/ssh start
fi

sleep 0.3

# install Screenshot
dpkg -l | grep shutter >> /dev/null
if [ $? -eq 0 ]
then
echo "Screenshot Is already installed \n"
else
sudo apt -y install shutter

sleep 0.3

# install filezilla
dpkg -l | grep filezilla >> /dev/null
if [ $? -eq 0 ]
then
echo "filezilla Is already installed \n"
else
sudo apt -y install filezilla
fi

sleep 0.3

# install ibus-pinyin
dpkg -l | grep ibus-pinyin >> /dev/null
if [ $? -eq 0 ]
then
echo "ibus-pinyin is already installed \n"
else
sudo apt -y install ibus-pinyin
fi

sleep 0.3

# install gnome-tweak-tool
dpkg -l | grep gnome-tweak-tool >> /dev/null
if [ $? -eq 0 ]
then
echo "gnome-tweak-tool is already installed \n"
else
sudo apt -y install gnome-tweak-tool
fi

sleep 0.3

# install smplayer
dpkg -l | grep smplayer >> /dev/null
if [ $? -eq 0 ]
then
echo "smplayer is already installed \n"
else
sudo apt -y install smplayer
fi

sleep 0.3

# install gimp
dpkg -l | grep gimp >> /dev/null
if [ $? -eq 0 ]
then
echo "gimp is already installed \n"
else
sudo apt -y install gimp
fi

sleep 0.3

# install Krita
dpkg -l | grep krita >> /dev/null
if [ $? -eq 0 ]
then
echo "Krita is already installed \n"
else
sudo apt -y install krita
fi

sleep 0.3

# install Google Chrome
dpkg -l | grep google-chrome-stable >> /dev/null
if [ $? -eq 0 ]
then
echo "google-chrome-stable is already installed \n"
else
sudo wget http://www.linuxidc.com/files/repo/google-chrome.list -P /etc/apt/sources.list.d/
wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add 
sudo apt update
sudo apt -y install google-chrome-stable
fi

sleep 0.3

# install Uget
dpkg -l | grep uget >> /dev/null
if [ $? -eq 0 ]
then
echo "uget Is already installed \n"
else
sudo apt -y install uget
sudo apt -y install aria2
fi

sleep 0.3

# install Uget -Chrome-wrapper
sudo add-apt-repository ppa:slgobinath/uget-chrome-wrapper
sudo apt update
sudo apt install uget-chrome-wrapper -y

sleep 0.3

# install axel
sudo apt install axel -y

sleep 0.3

# install Teamviewer
dpkg -l | grep teamviewer >> /dev/null
if [ $? -eq 0 ]
then
echo "teamviewer is already installed \n"
else
wget -O teamviewer.deb 'https://download.teamviewer.com/download/linux/teamviewer_amd64.deb'
sudo dpkg -i teamviewer.deb
sudo apt -f install -y 
sudo rm -f teamviewer.deb 
fi

sleep 0.3

# install netease-cloud-music
dpkg -l | grep netease-cloud-music >> /dev/null
if [ $? -eq 0 ]
then
echo "netease-cloud-music is already installed \n"
else
wget -O netease-cloud-music.deb 'http://d1.music.126.net/dmusic/netease-cloud-music_1.2.1_amd64_ubuntu_20190428.deb'
sudo dpkg -i netease-cloud-music.deb
sudo apt -f install -y
sudo rm -f netease-cloud-music.deb
fi

sleep 0.3

# install baidunetdisk
dpkg -l | grep baidunetdisk >> /dev/null
if [ $? -eq 0 ]
then
echo "baidunetdisk is already installed \n"
else
wget -O baidunetdisk.deb 'http://issuecdn.baidupcs.com/issue/netdisk/LinuxGuanjia/2.0.2/baidunetdisk_linux_2.0.2.deb'
sudo dpkg -i baidunetdisk.deb
sudo apt -f install -y
sudo rm -f baidunetdisk.deb
fi

sleep 0.3

# install WpsOffice
dpkg -l | grep wps-office >> /dev/null
if [ $? -eq 0 ]
then
echo "wps-office is already installed \n"
else
wget -O wpsoffice.deb 'https://wdl1.cache.wps.cn/wps/download/ep/Linux2019/9126/wps-office_11.1.0.9126_amd64.deb'
sudo dpkg -i wpsoffice.deb
sudo apt -f install -y
sudo rm -f wpsoffice.deb
fi

sleep 0.3

# install linuxqq
dpkg -l | grep linuxqq >> /dev/null
if [ $? -eq 0 ]
then
echo "linuxqq is already installed \n"
else
wget -O linuxqq.deb 'http://down.qq.com/qqweb/LinuxQQ/%E5%AE%89%E8%A3%85%E5%8C%85/linuxqq_2.0.0-b2-1076_amd64.deb'
sudo dpkg -i linuxqq.deb
sudo apt -f install -y
sudo rm -f linuxqq.deb
fi

sleep 0.3

#install typora
dpkg -l | grep typora >> /dev/null
if [ $? -eq 0 ]
then
echo "typora Is already installed \n"
else
wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add -
sudo add-apt-repository 'deb https://typora.io/linux ./'
sudo apt update
sudo apt install typora -y
fi

sleep 0.3

# install wine
dpkg -l | grep wine-stable >> /dev/null
if [ $? -eq 0 ]
then
echo "wine-stable is already installed \n"
else
sudo dpkg --add-architecture i386
sudo apt update
sudo apt install wine-stable -y
fi

sleep 0.3

# install wine-wechat
dpkg -l | grep wine-wechat >> /dev/null
if [ $? -eq 0 ]
then
echo "wine-wechat is already installed \n"
else
wget -O wine-wechat.deb 'http://www.ubuntukylin.com/public/pdf/wine-wechat_1.0-windows2.8.6_all.deb'
sudo dpkg -i wine-wechat.deb
sudo rm -f wine-wechat.deb
fi

sleep 0.3

# install finalshell
rm -f finalshell_install_linux.sh ;wget www.hostbuf.com/downloads/finalshell_install_linux.sh;chmod +x finalshell_install_linux.sh;./finalshell_install_linux.sh;

sleep 0.3

echo "\033[33m##############################################\033[0m"
echo "\033[33m#             \033[39munInstall Software             \033[33m#\033[0m"
echo "\033[33m##############################################\033[0m"

sleep 0.3

# remove LibreOffice

sudo apt remove libreoffice-common -y

sudo apt remove libreoffice-writer -y

sudo apt remove libreoffice-draw -y

sudo apt remove libreoffice-calc -y

# remove amazon
sudo apt remove unity-webapps-common -y

# autoremove

sudo apt autoremove -y


sleep 0.3

:<<!

# install VNC-Server
dpkg -l | grep realvnc-vnc-server >> /dev/null
if [ $? -eq 0 ]
then
echo "VNC-Server is already installed \n"
else
wget -O VNC-Server.deb "https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.6.0-Linux-x64.deb"
sudo dpkg -i VNC-Server.deb
sudo apt -f install -y
rm -f VNC-Server.deb
fi


sleep 0.3


# install flashplugin
dpkg -l | grep flashplugin-installer >> /dev/null
if [ $? -eq 0 ]
then
echo "flashplugin-installer is already installed \n"
else
sudo apt -y install flashplugin-installer
fi

!

# upgrade
sudo apt upgrade -y

echo "\n\033[33m#################### Finish ####################\033[0m \n "
