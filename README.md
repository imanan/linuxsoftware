## 介绍

Linux软件安装脚本可以在debian、ubuntu等各种发行版上进行常用软件的自动安装，避免重装系统后软件又重新安装一遍常用软件的烦恼，喝个茶看个剧回来常用软件统统安装搞定。


## 使用说明


在Linux终端中输入命令： 

```
#第一步必须先安装 Git
sudo apt-get install git

#抓取脚本文件
git clone https://gitee.com/imanan/linuxsoftware 

#进入脚本目录 
cd linuxsoftware

#修改163镜像源 For ubuntu18.04 不想修改的请忽略此步
sudo sh 163source.sh 

#安装软件
sudo sh install.sh

#安装开发和其他软件
sudo sh developsoftware.sh
```

## 软件源

目前只设置了网易163源，后期会增加其他源镜像选择


## 软件列表

openssh-server    让远程主机可以通过网络访问sshd服务

filezilla    Ftp客户端

ibus-pinyin    ibus拼音

gnome-tweak-tool    gnome系统优化工具

Smplayer    视频播放器

gimp    图片处理软件-PS的替代软件

flashplugin    flash插件

Google Chrome    谷歌浏览器

Teamviewer    远程控制软件

VNC-viewer-server   老牌远程软件

netease-cloud-music 网易云音乐

baidunetdisk    百度网盘

wpsoffice    WPSOffice非常好用

wine    wine 环境

Winewechat    微信wine版

Linuxqq    腾讯QQ官方版

Typroa    很不错的Markdown编辑器


## 开发软件以及下载速度较慢的软件

~~ 建议在所有软件安装完后安装 ~~

Dingtalk    阿里的钉钉

snap    snap安装源

Vscode    强大的Visual Studio Code编辑器

Atom    Github提供的Atom代码编辑器

Sublime Text 3  很不错的代码编辑器


## 软件卸载

liberoffice  装了wps就不用这个了而且使用也不太习惯，卸载了吧！

Amazon 没啥用直接砍了



## 系统更新重启

安装完所有软件后系统更新并重启系统


## 最后要说的是

本人非常爱折腾系统所以做了这个脚本，各种系统都喜欢尝尝鲜，导致装完系统后就又得重装好多软件。
这里有些软件安装包是直接从官方下载的，链接可能会失效，总之我会不定期更新


