#!/bin/bash

#install typora
dpkg -l | grep typora >> /dev/null
if [ $? -eq 0 ]
then
echo "openssh-server Is already installed"
else
apt-key adv –keyserver keyserver.ubuntu.com –recv-keys BA300B7755AFCFAE
add-apt-repository ‘deb https://typora.io ./linux/’
apt-get update
apt-get install typora
fi

#install VNC-Server
dpkg -l | grep VNC-Server >> /dev/null
if [ $? -eq 0 ]
then
echo "teamviewer is already installed"
else
wget -O VNC-Server.deb "https://www.realvnc.com/download/file/vnc.files/VNC-Server-6.6.0-Linux-x64.deb"
dpkg -i VNC-Server.deb
apt-get -f install 
rm -f VNC-Server.deb
fi




