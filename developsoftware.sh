#!/bin/bash

# install DingTalk
dpkg -l | grep dingtalk >> /dev/null
if [ $? -eq 0 ]
then
echo "dingtalk is already installed"
else
wget -O dingtalk.deb 'https://github.com/nashaofu/dingtalk/releases/download/v2.1.1/dingtalk-2.1.1-latest-amd64.deb'
sudo dpkg -i dingtalk.deb
sudo apt-get -f install -y
sudo rm -f dingtalk.deb
fi

sleep 0.3

# install Visual Studio Code
wget -O vscode.deb 'https://vscode.cdn.azure.cn/stable/8795a9889db74563ddd43eb0a897a2384129a619/code_1.40.1-1573664190_amd64.deb'
dpkg -i vscode.deb
apt-get -f install -y
rm -f vscode.deb

echo "##############################################"
echo "#        Start Install Snap Software         #"
echo "##############################################"

sleep 0.3

# install snapd
dpkg -l | grep -w snapd >> /dev/null
if [ $? -eq 0 ]
then
echo "snapd is already installed"
else
sudo apt-get -y install snapd
fi

sleep 0.3

# install Atom
dpkg -l | grep atom >> /dev/null
if [ $? -eq 0 ]
then
echo "atom is already installed"
else
sudo snap install atom --classic
fi

# install sublime-text
dpkg -l | grep sublime-text >> /dev/null
if [ $? -eq 0 ]
then
echo "sublime-text is already installed"
else
sudo snap install sublime-text --classic
fi

sleep 0.3

